When making a bug report please include the answers to these questions:

1. What game version?
2. What operating system?
3. What did you do?
4. What did you expect to happen?
5. What happen?

If possible please include a screenshot or video. This can be done by pressing
CTRL-9 while the game is running.

**For suggestions and feedback**: please make a comment on [Infiniraft's itch.io][inf_itch_io] page instead of opening an issue.

[inf_itch_io]: https://ldjq.itch.io/infiniraft "Infiniraft's itch.io page"