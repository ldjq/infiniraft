For feedback, feature requests, or enhancement suggests please make a comment on [itch.io][inf_itch_io] instead of opening an issue.

[inf_itch_io]: https://ldjq.itch.io/infiniraft "Infiniraft's itch.io page"