**Checklist:**
1. I have read [CONTRIBUTING.md][contributing]: NO.
2. I have discussed these changes on the issue tracker: NO (#ISSUE NUMBER).
3. I created a feature branch off of *dev*: NO.
4. I am asking to have my feature branch merged into *dev*: NO.
5. I have tested my changes: NO.
6. I have updated the changelog: NO.
7. I have added myself to the credits: NO.

[contributing]: https://gitlab.com/ldjq/infiniraft/-/blob/main/CONTRIBUTING.md "Infiniraft's guidelines for contributing"