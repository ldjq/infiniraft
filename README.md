# Infiniraft
You are trapped on a constantly moving raft, and you need to avoid taking damage while you collect as many coins as possible.

Originally made for [Ludum Dare 49][ludum_dare] Infiniraft is an arcade-style game made using [PICO-8][pico_8_site].

![Screenshot of Infiniraft gameplay](screenshot.gif)

## Installation
Infiniraft can be played in the browser from its [itch.io][inf_itch_io] page.

Downloads for Linux, macOS, Raspberry Pi (32-bit ARM), and Windows are also available (currently only for beta version).

### Beta
Beta versions of Infiniraft are a way for players to test upcoming releases, they are playable but usually lack polish.

Beta releases can be downloaded from the [releases page][beta_releases] on Infiniraft's GitLab.

*For a list of new changes read the “Unreleased” section of the CHANGELOG&period;md file.*

### Alpha
**You need to have [PICO-8][pico_8_site] installed to play the alpha version.**

If you want to try the latest features or to help find bugs then Infiniraft's alpha version can be downloaded [here][alpha_download].

The *infiniraft.p8* file needs to be moved into PICO-8's drive folder, which can be opened by launching PICO-8 then running the `FOLDER` command.
See the [PICO-8 manual][pico_8_vhd] for more details.

After moving the *infiniraft.p8* file start PICO-8, then load Infiniraft by entering in `LOAD INFINIRAFT.P8`.
Finally, use `RUN` to start the game.

*For a list of new changes read the “Unreleased” section of the [CHANGELOG.md][changelog] file.*

## Support
Encountered a bug while playing Infiniraft?
Please open an [issue][bug_report], or if you do not have a GitLab account you can post a comment on [itch.io][inf_itch_io].

Have feedback or would like to request a feature?
Leave a comment on [itch.io][inf_itch_io].

## Roadmap
Upcoming features and improvements are tracked using the [issue tracker][issue_tracker].
The “Unreleased” section of the [CHANGELOG.md][changelog] file lists new changes.

## Contributing
Want to help make Infiniraft?
Read the [CONTRIBUTING.md][contributing] file to get started.

It contains useful information like:
* What kind of help we are looking for.
* How to start making changes.
* How to submit your changes.

**A copy of PICO-8 is needed to make changes to Infiniraft's source code.**

## Acknowledgments
The [CREDITS.md][credits] file contains information about all of the people who have helped make Infiniraft.

## License
Infiniraft's source code is licensed under the GNU Affero General Public License.

Infiniraft's media assets (such as sprites, sound effects, and music) are licensed under the Attribution-ShareAlike 4.0 International license.

*See the [COPYING.txt][license] file for the full licenses.*

[alpha_download]: https://gitlab.com/ldjq/infiniraft/-/raw/main/src/infiniraft.p8?inline=false "Download the latest version of Infiniraft"
[beta_releases]: https://gitlab.com/ldjq/infiniraft/-/releases "Infiniraft's beta releases"
[bug_report]: https://gitlab.com/ldjq/infiniraft/-/issues/new?issuable_template=bug_report "File a bug report for Infiniraft"
[changelog]: https://gitlab.com/ldjq/infiniraft/-/blob/main/CHANGELOG.md "Infiniraft's changelog"
[contributing]: https://gitlab.com/ldjq/infiniraft/-/blob/main/CONTRIBUTING.md "Infiniraft's guidelines for contributing"
[credits]: https://gitlab.com/ldjq/infiniraft/-/blob/main/CREDITS.md "Infiniraft's credits"
[inf_itch_io]: https://ldjq.itch.io/infiniraft "Infiniraft's itch.io page"
[issue_tracker]: https://gitlab.com/ldjq/infiniraft/-/issues "Infiniraft's issue tracker"
[license]: https://gitlab.com/ldjq/infiniraft/-/blob/main/COPYING.txt "Infiniraft's licenses"
[ludum_dare]: https://ldjam.com/ "Official Ludum Dare website"
[pico_8_site]: https://www.lexaloffle.com/pico-8.php "Official PICO-8 website"
[pico_8_vhd]: https://www.lexaloffle.com/dl/docs/pico-8_manual.html#_File_System "Where the drive folder is on different platforms"
