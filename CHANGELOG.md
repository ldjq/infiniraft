# Changelog for Infiniraft
All notable changes to Infiniraft will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-11-09
### Added
- CONTRIBUTING with guidelines and information for getting started.
- COPYING with information about Infiniraft's license(s).
- CREDITS with information about those who helped make Infiniraft.
- Makefile for building Infiniraft for various platforms.
- README with install instructions and other information.
- The annotated version of the source code.
- This changelog.

[Unreleased]: https://gitlab.com/ldjq/infiniraft/-/compare?from=dev&to=main
[1.0.0]: https://gitlab.com/ldjq/infiniraft/-/tags/1.0.0
