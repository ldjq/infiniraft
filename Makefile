# Infiniraft
# Copyright (C) 2021 LDJQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ## About
# This program is used to build and package Infiniraft for various platforms.
#
# While this program gets the job done it is not what you would call elegant, so
# do not expect to have fun trying to understand it.
#
# Well, that is the joy of gluing different programs together where each one has
# its own ideas on how the world should work.
#
# One more thing: this works on Linux, and it might work on macOS.
# For Windows you could try running it using Windows Subsystem for Linux (WSL),
# but heavy modification of the program will probably be required.
#
# ## Dependencies
# * Make[^1].
# * PICO-8: this program assumes that the PICO-8 executable is on your PATH[^2]
#   and that it is named *pico8*. If this is not the case change the `pico8`
#   variable below to the correct command.
# * Zip: Info-ZIP's[^3] *zip* utility (or a compatible alternative) needs to be
#   installed.
#
# ## Usage
# In the root of the repository run `make build` to build Infiniraft for various
# platforms. This will create two directories: *build* acts as a cache to
# speedup later builds, and *dist* contains the ZIP files to distribute.
#
# Use `make clean` to remove the files created by `make build`. Doing this will
# cause the next build to take longer but it might be necessary to do if a
# build fails.
#
# To recap: run `make build` in the repository root, then distribute the ZIP
# archives in the *dist* directory.
#
#
# ## Notes for maintainers
#
# The information below should hopefully help explain why the code does some of
# the things it does.
#
# ### In the build directory what is the infiniraft.bin directory used for?
# It is not possible to have PICO-8 build binaries for platforms separately, nor
# is it possible to control the output name.
#
# The output directory always ends in *.bin*, and binaries for all platforms are
# built and placed in the output directory.
#
# ### What is up with all the file and directory renaming/copying?
# Info-ZIP's Zip assumes that the directory structure on disk is the
# same as that in the ZIP file. Meaning you cannot put *./file1* into the ZIP
# file as *./folder1/file1*. Instead you must create *folder1* and move *file1*
# into it, then you can add it.
#
# The root of a ZIP file should be a directory, because no one likes unzipping
# a ZIP file and having its contents spill out into the containing directory.
# Having a directory as the root means unzipping the archive makes a single
# directory with all the archive's contents.
#
# Zip makes this annoying to do. Each time you want to add or update
# a file you need to recreate the ZIP file's structure on disk. That is why the
# code does so much renaming and copying.
#
# ### Why am I getting a zip warning?
# The way PICO-8 creates ZIP files causes Zip to warn that the local version
# of the ZIP spec needed to extract a file does not match the version listed in
# the central directory entry for that file.
#
# Despite the warning it should not cause any problems, and the warning can be
# safely ignored.
#
# Below will give more information on why PICO-8 creates ZIP files this way.
#
# The PICO-8 manual states:
# > Windows file systems do not support the file metadata needed to create a
# > Linux or Mac executable. PICO-8 works around this by exporting zip files in
# > a way that preserves the file attributes. It is therefore recommended that
# > you distribute the outputted zip files as-is to ensure users on other
# > operating systems can run them. Otherwise, a Linux user who then downloads
# > the binaries may need to "chmod +x mygame" the file to run it, and Mac user
# > would need to "chmod +x mygame.app/Contents/MacOS/mygame"
#
# The above explains why ZIP files are created but does not say that they are
# created by streaming data to the archive. Using Info-ZIP's *zipdetails*
# utility shows this to be the case.
#
# When a file is added to a ZIP archive a local file header[^4] is created. The
# local file header has a "Version needed to extract (minimum)" field that needs
# to be set before the file data can be written. Because the data is streamed,
# PICO-8 does not know the value to set until after the file is added; so PICO-8
# sets the value to 2.0 as an initial guess.
#
# Once all files have been added a central directory entry[^5] is made for each
# file. At this point PICO-8 knows the actual version needed, and PICO-8 uses it
# for the "Version needed to extract (minimum)" field of the central directory
# entries.
#
# The mismatch arises when the guessed version and the actual version differ.
# The local file header and central directory entry will list different minimum
# versions.
#
# Unzipping utilities should use the version listed in the central directory
# entry, so they should not have any problems.
#
# Zip issues a warning because this could be a sign of file corruption, but
# that is not true in this case.
#
# [^1]: https://en.wikipedia.org/wiki/Make_(software)
# [^2]: https://en.wikipedia.org/wiki/PATH_(variable)
# [^3]: https://en.wikipedia.org/wiki/Info-ZIP
# [^4]: https://en.wikipedia.org/wiki/ZIP_(file_format)#Local_file_header
# [^5]: https://en.wikipedia.org/wiki/ZIP_(file_format)#Central_directory_file_header
#

pico8 = 'pico8'

.DELETE_ON_ERROR:
.PHONY: build clean

build: ./build/infiniraft.bin/infiniraft_linux.zip ./build/CHANGELOG.md ./build/COPYING.txt ./build/CREDITS.md ./build/README.md ./dist/infiniraft_linux.zip

clean:
	rm --force --recursive ./build/infiniraft.bin/
	rm --force ./build/CHANGELOG.md
	rm --force ./build/COPYING.txt
	rm --force ./build/CREDITS.md
	rm --force ./build/README.md
	rm --dir --force ./build/
	
	rm --force ./dist/infiniraft_html.zip
	rm --force ./dist/infiniraft_linux.zip
	rm --force ./dist/infiniraft_osx.zip
	rm --force ./dist/infiniraft_raspi.zip
	rm --force ./dist/infiniraft_windows.zip
	rm --dir --force ./dist/

./build/infiniraft.bin/infiniraft_linux.zip: ./src/infiniraft.p8
	mkdir --parents ./build/
	
	# Delete previous builds to prevent removed files from sticking around:
	rm --force --recursive ./build/infiniraft.bin/
	
	cd ./build/ && ${pico8} ../src/infiniraft.p8 -export infiniraft.bin
	cd ./build/infiniraft.bin/ && ${pico8} ../../src/infiniraft.p8 -export '-f infiniraft.html'
	cd ./build/infiniraft.bin/ && zip --quiet --recurse-paths infiniraft_html ./infiniraft_html/
	
	# The directory names PICO-8 uses in the ZIP files are different than those
	# it leaves unzipped, so we rename them:
	mv ./build/infiniraft.bin/linux/ ./build/infiniraft.bin/infiniraft_linux/
	mv ./build/infiniraft.bin/raspi/ ./build/infiniraft.bin/infiniraft_raspi/
	mv ./build/infiniraft.bin/windows/ ./build/infiniraft.bin/infiniraft_windows/

./build/CHANGELOG.md: ./build/infiniraft.bin/infiniraft_linux/infiniraft CHANGELOG.md
	cp --force ./CHANGELOG.md ./build/CHANGELOG.md
	
	( \
	set -e ;\
	cd ./build/ ;\
	target=$$(realpath ./CHANGELOG.md) ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_html/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_linux/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft.app/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_raspi/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_windows/)" ;\
	)
	
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_html ./infiniraft_html/CHANGELOG.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_linux ./infiniraft_linux/CHANGELOG.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_osx ./infiniraft.app/CHANGELOG.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_raspi ./infiniraft_raspi/CHANGELOG.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_windows ./infiniraft_windows/CHANGELOG.md

./build/COPYING.txt: ./build/infiniraft.bin/infiniraft_linux/infiniraft COPYING.txt
	cp --force ./COPYING.txt ./build/COPYING.txt
	
	( \
	set -e ;\
	cd ./build/ ;\
	target=$$(realpath ./COPYING.txt) ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_html/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_linux/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft.app/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_raspi/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_windows/)" ;\
	)
	
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_html ./infiniraft_html/COPYING.txt
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_linux ./infiniraft_linux/COPYING.txt
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_osx ./infiniraft.app/COPYING.txt
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_raspi ./infiniraft_raspi/COPYING.txt
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_windows ./infiniraft_windows/COPYING.txt

./build/CREDITS.md: ./build/infiniraft.bin/infiniraft_linux/infiniraft CREDITS.md
	cp --force ./CREDITS.md ./build/CREDITS.md
	
	( \
	set -e ;\
	cd ./build/ ;\
	target=$$(realpath ./CREDITS.md) ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_html/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_linux/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft.app/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_raspi/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_windows/)" ;\
	)
	
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_html ./infiniraft_html/CREDITS.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_linux ./infiniraft_linux/CREDITS.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_osx ./infiniraft.app/CREDITS.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_raspi ./infiniraft_raspi/CREDITS.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_windows ./infiniraft_windows/CREDITS.md

./build/README.md: ./build/infiniraft.bin/infiniraft_linux/infiniraft README.md
	cp --force ./README.md ./build/README.md
	
	( \
	set -e ;\
	cd ./build/ ;\
	target=$$(realpath ./README.md) ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_html/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_linux/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft.app/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_raspi/)" ;\
	ln --symbolic --force "$$target" "$$(realpath ./infiniraft.bin/infiniraft_windows/)" ;\
	)
	
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_html ./infiniraft_html/README.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_linux ./infiniraft_linux/README.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_osx ./infiniraft.app/README.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_raspi ./infiniraft_raspi/README.md
	cd ./build/infiniraft.bin/ && zip --quiet infiniraft_windows ./infiniraft_windows/README.md

./dist/infiniraft_linux.zip: ./build/infiniraft.bin/infiniraft_html.zip
	mkdir --parents ./dist/
	
	cp --force ./build/infiniraft.bin/infiniraft_html.zip ./dist/
	cp --force ./build/infiniraft.bin/infiniraft_linux.zip ./dist/
	cp --force ./build/infiniraft.bin/infiniraft_osx.zip ./dist/
	cp --force ./build/infiniraft.bin/infiniraft_raspi.zip ./dist/
	cp --force ./build/infiniraft.bin/infiniraft_windows.zip ./dist/
