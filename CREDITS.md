# Credits for Infiniraft

## Core contributors (alphabetical)
* LDJQ ([itch.io](https://ldjq.itch.io/))

## Contributors (alphabetical)

[You could be here!](https://gitlab.com/ldjq/infiniraft/-/blob/main/CONTRIBUTING.md)

## Third parties (alphabetical)
* GitLab for source code hosting ([website](https://about.gitlab.com/))
* Igara Studio S.A. for making Aseprite ([website](https://www.aseprite.org/))
* itch.io for hosting ([website](https://itch.io/))
* Lexaloffle for making PICO-8 ([website](https://www.lexaloffle.com/))
