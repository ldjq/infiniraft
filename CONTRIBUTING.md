# Contributing guidelines for Infiniraft

## Introduction
Hey, thank you for considering contributing to Infiniraft.
It is with the help of people like you that Infiniraft can be made into a great game.

By following the guidelines given in this document you are insuring your time and other people's time is not wasted.
You are also increasing the likelihood that your changes are accepted.

In order to avoid turning into a chaotic mess Infiniraft must follow some kind of vision.
Not all changes fit that vision, so before any change is made it should be discussed on the [issue tracker][issue_tracker].

*You can look over both the open and closed issues to get a feel for Infiniraft's vision.*

### Ground rules
It should go without saying that everyone in the Infiniraft community should be respectful and considerate of others.
It should also go without saying that Infiniraft's community is not the place for flame wars or political discussion.

On the technical side of things changes are expected to be:
* Cross-platform: Infiniraft should work on Linux, macOS, and Windows.
* Well tested and bug free.
* Discussed before being made.

## Making a contribution
The instructions below assume that you have a basic understanding of Git.
If that is not the case [this course][git_course] can get you up to speed, and [this site][newbie_issues] lists projects that have help for newbies.

Once you are up to speed: browse the issue tracker for issues labeled [easy][easy_issues].

### Getting started

*Before doing anything make sure you have [PICO-8][pico_8_site] installed.*

First, either open a new issue to discuss the changes you would like to make or ask to be assigned to an open issue.

Next you need to [fork][fork_repo] Infiniraft's repository; this way you can test your changes before submitting them.

Clone your fork into PICO-8's drive folder.
You can use PICO-8's `FOLDER` command to open the drive folder in your file browser, and the [PICO-8 manual][pico_8_vhd] has more information about file system commands.

To keep in sync with Infiniraft's repository you need to add it as *upstream*: `git remote add upstream 'git@gitlab.com:ldjq/infiniraft.git'`.
Verify this worked by running: `git remote --verbose`.

All work on Infiniraft happens in feature branches made off of the *dev* branch, but currently your fork only has the *main* branch.
Create the *dev* branch by doing the following:
1. Get any new changes: `git fetch upstream`
2. Create and checkout *dev*: `git checkout -b dev --track upstream/dev`
3. Push *dev* to your fork: `git push origin dev`

To start working: from the *dev* branch create a feature branch by running `git checkout -b FEATURE` where *FEATURE* is a descriptive name for your changes.

Now you can make and commit your changes.

Update [CHANGELOG.md][changelog] by adding a description of your changes to the “Unreleased” section.
The link in the file explains what to add.

If you have not already added yourself to the “Contributors” section of [CREDITS.md][credits] do so now.
Please keep the list alphabetical, and only add a couple of links to where people can contact you.

Once satisfied with your work you can push the feature branch to your fork.

Make a merge request to have your feature branch merged into the *dev* branch of Infiniraft's repository.

After your merge request is accepted you need to sync with Infiniraft's repository again:
1. Checkout *dev*: `git checkout dev`
2. Get the changes: `git fetch upstream`
3. Apply the changes: `git rebase upstream/dev`
4. Push the changes to your fork: `git push origin dev`

Congratulations! You have made your first contribution to Infiniraft.
Eventually your changes will be merged into the alpha *main* branch, then into a stable release.

## Bug reports, feature suggests, and improvements
If you encounter a bug: either open an [issue][bug_report] or make a comment on [itch.io][inf_itch_io].

> When making a bug report please include the answers to these questions:
>
> 1. What game version?
> 2. What operating system?
> 3. What did you do?
> 4. What did you expect to happen?
> 5. What happen?
>
> If possible please include a screenshot or video. This can be done by pressing
> CTRL-9 while the game is running.
>
> **For suggestions and feedback**: please make a comment on [Infiniraft's itch.io][inf_itch_io] page instead of opening an issue.

For feature or enhancement suggests make a comment on [itch.io][inf_itch_io].
If your suggestion fits the project's vision an issue will be opened where it will be further discussed.

## Community
If there is enough interest a discussion board can be created.
Show your interest by leaving a comment on Infiniraft's [itch.io][inf_itch_io] page.

[bug_report]: https://gitlab.com/ldjq/infiniraft/-/issues/new?issuable_template=bug_report "File a bug report for Infiniraft"
[changelog]: https://gitlab.com/ldjq/infiniraft/-/blob/main/CHANGELOG.md "Infiniraft's changelog"
[credits]: https://gitlab.com/ldjq/infiniraft/-/blob/main/CREDITS.md "Infiniraft's credits"
[easy_issues]: https://gitlab.com/ldjq/infiniraft/-/issues?label_name=Easy "Infiniraft issues labeled easy"
[fork_repo]: https://gitlab.com/ldjq/infiniraft/-/forks/new "Fork Infiniraft"
[git_course]: https://egghead.io/courses/how-to-contribute-to-an-open-source-project-on-github "egghead.io"
[inf_itch_io]: https://ldjq.itch.io/infiniraft "Infiniraft's itch.io page"
[issue_tracker]: https://gitlab.com/ldjq/infiniraft/-/issues "Infiniraft's issue tracker"
[newbie_issues]: https://www.firsttimersonly.com/ "First Timers Only"
[pico_8_site]: https://www.lexaloffle.com/pico-8.php "PICO-8's official website"
[pico_8_vhd]: https://www.lexaloffle.com/dl/docs/pico-8_manual.html#_File_System "Where the drive folder is on different platforms"
